//
//  TMWebViewController.h
//  Timi
//
//  Created by 潘龙 on 2017/10/9.
//  Copyright © 2017年 LaiYoung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMWebViewController : UIViewController
- (instancetype)initWithUrl:(NSString *)url;
@end
