//
//  TMWebViewController.m
//  Timi
//
//  Created by 潘龙 on 2017/10/9.
//  Copyright © 2017年 LaiYoung. All rights reserved.
//

#import "TMWebViewController.h"

@interface TMWebViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *currentWeb;
@property (nonatomic, copy)NSString *url;

@end

@implementation TMWebViewController
- (instancetype)initWithUrl:(NSString *)url
{
    if (self = [super init]) {
        _url = url;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.currentWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
