//
//  ViewController.m
//  XFY
//
//  Created by 潘龙 on 2017/11/8.
//  Copyright © 2017年 panlong. All rights reserved.
//

#import "ViewController.h"

#define Width   self.view.bounds.size.width
#define Height  self.view.bounds.size.height
@interface ViewController ()<UIWebViewDelegate>
@property(nonatomic,strong)UIWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  loadWKWebView];

}
-(void)loadWKWebView
{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, Width, Height-20)];
    
    _webView.scrollView.bounces = NO;
    
    _webView.delegate = self;
    
    _webView.backgroundColor = [UIColor whiteColor];
    
    //    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://47.92.141.204:8881"]]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://xfy178.cnhv6.hostpod.cn/"]]];
    
    //wap.taolvtuan.com
    //http://caipiao.cnhv7.hostpod.cn/Cp/index/index.html
    //http://47.92.141.204:8881
    [self.view addSubview:_webView];
    
    //    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    btn.frame =CGRectMake(100,100, 100, 100);
    //    [btn setTitle:@"1239u12udsdfghjke" forState:(UIControlStateNormal)];
    //    [btn setBackgroundColor:[UIColor redColor]];
    //    [btn addTarget: self action:@selector(clicks) forControlEvents:(UIControlEventTouchUpInside)
    //     ];
    //    [self.view addSubview:btn];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // NOTE: ------  对alipays:相关的scheme处理 -------
    // NOTE: 若遇到支付宝相关scheme，则跳转到本地支付宝App
    NSString* reqUrl = request.URL.absoluteString;
    if ([reqUrl hasPrefix:@"alipays://"] || [reqUrl hasPrefix:@"alipay://"]) {
        // NOTE: 跳转支付宝App
        BOOL bSucc = [[UIApplication sharedApplication]openURL:request.URL];
        
        // NOTE: 如果跳转失败，则跳转itune下载支付宝App
        if (!bSucc) {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示"
//                                                           message:@"未检测到支付宝客户端，请安装后重试。"
//                                                          delegate:self
//                                                 cancelButtonTitle:@"立即安装"
//                                                 otherButtonTitles:nil];
//            [alert show];
        }
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
