//
//  ViewController.h
//  大件吊运
//
//  Created by 潘龙 on 2017/10/26.
//  Copyright © 2017年 panlong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
@protocol JSObjcDelegate <JSExport>
- (void)callqq;
- (void)callweixin;
- (void)callweibo;
- (void)getCall:(NSString *)callString;
//- (void)qqResult:(NSString *)openId headPic(NSString *)headPic nickName:(NSString *)nickName;
@end

@interface ViewController : UIViewController

@property (nonatomic, strong) JSContext *jsContext;
- (void)getCall:(NSString *)url;

- (instancetype)initWithUrl:(NSString *)url;
@end

