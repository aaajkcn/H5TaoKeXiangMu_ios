//
//  main.m
//  大件吊运
//
//  Created by 潘龙 on 2017/10/26.
//  Copyright © 2017年 panlong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
