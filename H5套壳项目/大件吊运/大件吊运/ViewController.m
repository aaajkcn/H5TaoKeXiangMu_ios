//
//  ViewController.m
//  大件吊运
//
//  Created by 潘龙 on 2017/10/26.
//  Copyright © 2017年 panlong. All rights reserved.
//

#import "ViewController.h"
#import <ShareSDK/ShareSDK.h>

#define Width   self.view.bounds.size.width
#define Height  self.view.bounds.size.height
@interface ViewController ()<UIWebViewDelegate,JSObjcDelegate>


@property(nonatomic,strong)UIWebView *webView;

@property(nonatomic,strong)UIView *headView;
@property (nonatomic, copy) NSString *url;
@end

@implementation ViewController

- (instancetype)initWithUrl:(NSString *)url
{
    if (self = [super init]) {
        
        self.url = url;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self  loadWKWebView];
}

-(void)loadWKWebView
{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, Width, Height-20)];
    
    _webView.scrollView.bounces = NO;
    
    _webView.delegate = self;
    
    _webView.backgroundColor = [UIColor whiteColor];
    
//    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://47.92.141.204:8881"]]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];

    //wap.taolvtuan.com
    //http://caipiao.cnhv7.hostpod.cn/Cp/index/index.html
    //http://47.92.141.204:8881
    [self.view addSubview:_webView];
    
//    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//    btn.frame =CGRectMake(100,100, 100, 100);
//    [btn setTitle:@"1239u12udsdfghjke" forState:(UIControlStateNormal)];
//    [btn setBackgroundColor:[UIColor redColor]];
//    [btn addTarget: self action:@selector(clicks) forControlEvents:(UIControlEventTouchUpInside)
//     ];
//    [self.view addSubview:btn];
}

- (void)clicks
{
    NSLog(@"click");
    [ShareSDK getUserInfo:(SSDKPlatformTypeWechat) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
    }];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    NSString *jsStr = [NSString stringWithFormat:@"wxLogin()"];
//    [_webView stringByEvaluatingJavaScriptFromString:jsStr];
    
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.jsContext[@"tianbai"] = self;
    self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        NSLog(@"异常信息：%@", exceptionValue);
    };

}

#pragma mark- UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//    NSURL * url = [request URL];
//    NSLog(@"%@",[url scheme]);
//    if ([[url scheme] isEqualToString:@"wxlogin"]) {
//        //获取微信信息
////        UIAlertView *alett = [[UIAlertView alloc] initWithTitle:@"wxlogin" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
////        [alett show];
//        NSArray *params =[url.query componentsSeparatedByString:@"&"];
//        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
//        for (NSString *paramStr in params) {
//            NSArray *dicArray = [paramStr componentsSeparatedByString:@"="];
//            if (dicArray.count > 1) {
//                NSString *decodeValue = [dicArray[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                [tempDic setObject:decodeValue forKey:dicArray[0]];
//            }
//        }
//        [ShareSDK getUserInfo:(SSDKPlatformTypeWechat) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
//            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
//        }];
//        return NO;
//    }
//    if ([[url scheme] isEqualToString:@"qqlogin"]) {
////        UIAlertView *alett = [[UIAlertView alloc] initWithTitle:@"qqlogin" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
////        [alett show];
//        //获取qq信息
//        [ShareSDK getUserInfo:(SSDKPlatformTypeQQ) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
//            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
//        }];
//        return NO;
//
//    }
//
//    if ([[url scheme] isEqualToString:@"weibologin"]) {
//        UIAlertView *alett = [[UIAlertView alloc] initWithTitle:@"weibologin" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
//        [alett show];
//        //获取微博信息
//        [ShareSDK getUserInfo:(SSDKPlatformTypeSinaWeibo) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
//            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
//        }];
//        return NO;
//
//    }
    return YES;
}

- (void)callqq{
    NSLog(@"call");
    
    [ShareSDK getUserInfo:(SSDKPlatformTypeQQ) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
            // 之后在回调js的方法Callback把内容传出去
            JSValue *Callback = self.jsContext[@"callqq"];
            //传值给web端
            [Callback callWithArguments:@[user.uid,user.icon,user.nickname]];

        }
    }];
}

- (void)callweixin
{
    [ShareSDK getUserInfo:(SSDKPlatformTypeWechat) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
            // 之后在回调js的方法Callback把内容传出去
            JSValue *Callback = self.jsContext[@"callweixin"];
            //传值给web端
            [Callback callWithArguments:@[user.uid,user.icon,user.nickname]];
            
        }
    }];
}


- (void)callweibo
{
    //获取微博信息
    [ShareSDK getUserInfo:(SSDKPlatformTypeSinaWeibo) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            NSLog(@"%@%@%@",user.uid,user.nickname,user.icon);
            // 之后在回调js的方法Callback把内容传出去
            JSValue *Callback = self.jsContext[@"callweibo"];
            //传值给web端
            [Callback callWithArguments:@[user.uid,user.icon,user.nickname]];
            
        }
    }];
}

- (void)getCall:(NSString *)url
{
    NSLog(@"%@",url);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
