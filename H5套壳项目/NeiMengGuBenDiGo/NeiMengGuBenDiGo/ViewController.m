//
//  ViewController.m
//  NeiMengGuBenDiGo
//
//  Created by 欧腾 on 2018/1/4.
//  Copyright © 2018年 outeng. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import "SVProgressHUD.h"
#define Width   [UIScreen mainScreen].bounds.size.width
#define Height  [UIScreen mainScreen].bounds.size.height
@interface ViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>
@property(nonatomic,strong)WKWebView*webView;
@property (nonatomic, assign) id<WKScriptMessageHandler> scriptDelegate;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  loadWKWebView];
    
}
-(void)loadWKWebView
{
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 20, Width, Height-20)];
    
    
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    self.webView.scrollView.bounces = NO;
    
    _webView.backgroundColor = [UIColor whiteColor];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD setFont:[UIFont systemFontOfSize:12]];
    [SVProgressHUD showWithStatus:@"正在加载"];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.usb119.cn"]]];
    
   
    
    
}

#pragma mark 网页代理
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    
    NSLog(@"页面加载完成");
    [SVProgressHUD dismiss];
    [self.view addSubview:_webView];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    // NOTE: ------  对alipays:相关的scheme处理 -------
    // NOTE: 若遇到支付宝相关scheme，则跳转到本地支付宝App
    NSString* reqUrl = request.URL.absoluteString;
    if ([reqUrl hasPrefix:@"alipays://"] || [reqUrl hasPrefix:@"alipay://"]) {
        // NOTE: 跳转支付宝App
        BOOL bSucc = [[UIApplication sharedApplication]openURL:request.URL];
        
        // NOTE: 如果跳转失败，则跳转itune下载支付宝App
        if (!bSucc) {
            //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示"
            //                                                           message:@"未检测到支付宝客户端，请安装后重试。"
            //                                                          delegate:self
            //                                                 cancelButtonTitle:@"立即安装"
            //                                                 otherButtonTitles:nil];
            //            [alert show];
        }
        return NO;
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
