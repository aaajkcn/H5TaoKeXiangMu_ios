//
//  ViewController.m
//  ShuiLi
//
//  Created by 欧腾 on 2017/8/4.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import "SVProgressHUD.h"
#import "UIColor+ColorChange.h"
#import <ShareSDK/ShareSDK.h>
#define Width   self.view.bounds.size.width
#define Height  self.view.bounds.size.height


@interface ViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *weixinBtn;
@property (weak, nonatomic) IBOutlet UIButton *qqBtn;
@property (weak, nonatomic) IBOutlet UIButton *weiboBtn;

@property(nonatomic,strong)UIWebView *webView;

@property(nonatomic,strong)UIView *headView;

@end

@implementation ViewController
- (IBAction)weixinClick:(id)sender {
    
    [ShareSDK getUserInfo:(SSDKPlatformTypeWechat) onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SVProgressHUD show];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self  loadWKWebView];
}

-(void)loadWKWebView
{
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, Width, Height)];
    
    _webView.scrollView.bounces = NO;
    _webView.delegate = self;
    _webView.backgroundColor = [UIColor whiteColor];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://47.92.141.204:8881"]]];
    //wap.taolvtuan.com
    //http://caipiao.cnhv7.hostpod.cn/Cp/index/index.html
    //http://47.92.141.204:8881
    [self.view addSubview:_webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
