//
//  CALayer+MyViewColor.m
//  MeimiaoNews
//
//  Created by 欧腾 on 2017/7/15.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "CALayer+MyViewColor.h"

@implementation CALayer (MyViewColor)

-(void)setBorderUIColor:(UIColor *)borderUIColor
{
    
    self.borderColor =  borderUIColor.CGColor;
}



-(UIColor*)borderUIColor
{
    
    return [UIColor colorWithCGColor:self.borderColor];
}


@end
