//
//  CALayer+MyViewColor.h
//  MeimiaoNews
//
//  Created by 欧腾 on 2017/7/15.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (MyViewColor)
@property(nonatomic,assign)UIColor *borderUIColor;
@end
