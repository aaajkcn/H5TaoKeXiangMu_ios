//
//  AppDelegate.h
//  YiDingShopping
//
//  Created by 欧腾 on 2017/8/22.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

