//
//  ViewController.m
//  XinFangMeng
//
//  Created by 欧腾 on 2017/7/31.
//  Copyright © 2017年 outeng. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import "SVProgressHUD.h"
#define Width   self.view.bounds.size.width
#define Height  self.view.bounds.size.height

@interface ViewController ()<UIWebViewDelegate>
//<WKUIDelegate,WKNavigationDelegate>
@property(nonatomic,strong)UIWebView *webView;
//@property(nonatomic,strong)WKWebView *webView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self  loadWKWebView];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)loadWKWebView
{
    [SVProgressHUD show];
    _webView =  [[UIWebView alloc]initWithFrame:CGRectMake(0, 20, Width, Height - 20)];
    //[[WKWebView alloc] initWithFrame:CGRectMake(0, 20, Width, Height-20)];
        _webView.delegate =self;
//    _webView.UIDelegate=self;
//    _webView.navigationDelegate=self;
    
    //    NSURLSessionDataTask
    // [ _webView.configuration.userContentController addScriptMessageHandler:self name:@"qqdl"];
    //http://appweb.outeng.net/
    //@"http://103.20.193.99:801/index1.html"
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://xinfangmeng.com/App/"]]];
    //http://103.20.193.99:801/index1.html
    //http://appweb.outeng.net/
    ///
    //http://cpcp2255.com/
    // [self.view addSubview:_webView];
    [SVProgressHUD dismiss];
    [self.view addSubview:_webView];

   
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    
    NSLog(@"页面加载完成");
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
